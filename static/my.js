jQuery(document).ready(function($){
	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	function csrfSafeMethod(method) {
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
	    beforeSend: function(xhr, settings) {
	        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
	            xhr.setRequestHeader("X-CSRFToken", csrftoken);
	        }
	    }
	});

});


$(function() {
$('.js-like').click(function(){
	var $btn = $(this);
	$.ajax({
		type: "POST",
		url: $btn.data('url'),
		data: {
			'vote': $btn.data('vote'),
			'id': $btn.data('id'),
			'liketype': $btn.data('liketype')
		},
		success: function(resp){			
			if (resp.status == false) {
				alert(resp.message);
			}
			else {							
				$btn.parent().find('.js_cnt').text(resp.new_rating);					
			}						
		},
		dataType: 'json'  
	});
	return false;
})
});


$(function() {
$('.js-isright').click(function(){
	var $btn = $(this);
	$.ajax({
		type: "POST",
		url: $btn.data('url'),
		data: {			
			'id': $btn.data('id')			
		},
		success: function(resp){			
			$btn.parent().parent().find('.js_correct').text(resp.message);
			$btn.prop("checked", resp.new_value)

		},
		dataType: 'json'  
	});
	return false;
})
});

