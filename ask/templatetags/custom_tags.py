from django import template
from ask.models import User,Tag

register = template.Library()

@register.inclusion_tag('popular_tags.html')
def get_popular_tags():
	#popular_tags = Tag.objects.filter(id__gte = 1200)
	popular_tags = Tag.objects.all()[0:30]
	
	return {'popular_tags' : popular_tags}

@register.inclusion_tag('best_members.html')
def get_best_members():
	#members = User.objects.order_by('-profile__rating').all()[0:10]
	#members = User.objects.filter(id__lte = 10)
	members = User.objects.all()[0:10]
	return {'members' : members}

@register.simple_tag
def url_replace(request, field, value):
	dict_ = request.GET.copy()
	dict_[field] = value
	return dict_.urlencode()
