from django.shortcuts import render
from django.http import HttpResponse
import os

def hello(request):

	output = "Hello, world!<br>"
	
	if request.method == "GET":
		output += "GET<br>"
		qd = request.GET	
	elif request.method == "POST":
		output += "POST<br>"
		qd = request.POST

	for k,v in qd.items():
		output += "%s = %s<br>" % (k,v) 

	return HttpResponse(output)


